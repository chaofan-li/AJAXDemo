<%--
  Created by IntelliJ IDEA.
  User: lichaofan
  Date: 2021/5/17
  Time: 10:41 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#uid").blur(function () {
            var uid = $(this).val();
            $.post("/getuser","uid="+uid, function (rs) {
                rs = eval("("+rs+")");
                //此时的rs是JSON格式的对象
                $("#uname").val(rs.username);
                $("#pass").val(rs.password);
                $("#money").val(rs.money);
            })
        })
    })

</script>

<body>
    uid:<input type="text" id="uid"><br>
    name:<input type="text" id="uname"><br>
    pass:<input type="text" id="pass"><br>
    money:<input type="text" id="money"><br>
</body>
</html>
