package servlet;

import bean.Users;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Author Clarklevis
 * @Date 2021/5/17
 */
@WebServlet(urlPatterns = "/getuser")
public class GetUsersServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uid = req.getParameter("uid");
        int userid = Integer.parseInt(uid);
        Users users = null;
        switch (userid){
            case 1:
                users = new Users(1,"张三","abc",11.212);
                break;
            case 2:
                users = new Users(2,"张三1","abc1",11.222);
                break;
            case 3:
                users = new Users(3,"张三2","abc2",11.232);
                break;
            case 4:
                users = new Users(4,"张三3","abc3",11.242);
                break;
            default:
                users = new Users();
        }

        //将java对象转化为JSON对象
        JSONObject jsonObject = JSONObject.fromObject(users);
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();
        writer.print(jsonObject);

    }
}
