<%--
  Created by IntelliJ IDEA.
  User: lichaofan
  Date: 2021/5/13
  Time: 9:45 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <script type="text/javascript">
    var xmlHttp;
    function test() {
      //发送异步请求
      //1 创建XMLHttpRequest对象
      if (window.XMLHttpRequest){
        //非IE
        xmlHttp = new XMLHttpRequest();
      }else if (window.ActiveXObject){
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      //2 打开链接
      var uname = document.getElementById("uname").value;
      xmlHttp.open("get","/testuname?username="+uname, true);
      //3 指定回调函数
      xmlHttp.onreadystatechange = function () {
      //3.1 判断状态
        if (xmlHttp.readyState = 4){
          var responseText = xmlHttp.responseText;
          document.getElementById("rs").innerText = responseText;
        }
      }
      //4 发送数据
      xmlHttp.send();


    }
  </script>
  <body>
  <h1>index.jsp</h1>
  username: <input type="text" id="uname" onblur="test()"><span id="rs"></span>
  <!--一旦鼠标离开输入框，就启动test方法，开始执行上面的发送请求-->
  </body>
</html>
